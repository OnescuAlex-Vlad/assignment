from openpyxl import load_workbook
from collections import defaultdict
import sqlite3

db = sqlite3.connect('investor_weekly_carloads.db')
cur = db.cursor()

cur.execute('''CREATE TABLE IF NOT EXISTS august_september
               (name text, value text)''')

investor_carload_august = 'investor-weekly-carloads-august-2016.xlsx'
investor_carload_september = 'investor-weekly-carloads-september-2016.xlsx'

investor_carloads = defaultdict(list)


def parse_xlsx(investor_car):
    wb = load_workbook(investor_car)
    for page in wb.sheetnames:
        ws = wb[page]

        for item in range(7, 31):
            key = ws[f'A{item}'].value
            value = ws[f'C{item}'].value
            if key is not None:
                investor_carloads[key].append(value)
        investor_carloads['week_start'].append(ws['A3'].value[19:29])
        investor_carloads['week_end'].append(ws['A3'].value[34:44])


def write_to_db(time_period):
    for ic_key, ic_value in investor_carloads.items():
        cur.execute(f"INSERT INTO {time_period} VALUES (?, ?)",
                    [str(ic_key), str(ic_value)])


parse_xlsx(investor_carload_august)
parse_xlsx(investor_carload_september)
write_to_db('august_september')

db.commit()
db.close()
