from flask import Flask, request
import sqlite3

app = Flask(__name__)


@app.route('/data/', methods=['GET'])
def get_weekly_carloads():
    conn = sqlite3.connect('investor_weekly_carloads.db')
    cursor = conn.execute('SELECT * FROM august_september')
    carloads = cursor.fetchall()
    conn.close()
    weekly_carloads = dict(carloads)
    query_result = dict()

    week_start = request.args['week_start']
    week_end = request.args['week_end']

    week_start_list = [x.replace("'", '') for x in weekly_carloads['week_start'].strip('][').split(', ')]
    week_start_index = week_start_list.index(week_start)

    week_end_list = [x.replace("'", '') for x in weekly_carloads['week_end'].strip('][').split(', ')]
    week_end_index = week_end_list.index(week_end)
    print(week_start_index)
    print(week_end_index)
    for key, value in weekly_carloads.items():
        if week_start_index == week_end_index:
            week_end_index += 1
        values = value.strip('][').split(', ')
        query_result[key] = values[week_start_index:week_end_index]

    return query_result


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
